﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;
using UnityEngine;


public class Database : MonoBehaviour {

	// Use this for initialization
	void Start () {
		string conn= "URI=file:" + Application.dataPath + "/database.db";
		IDbConnection dbConn = (IDbConnection) new SqliteConnection(conn);
		dbConn.Open(); //Open connection to the database.
		IDbCommand dbcmd = dbConn.CreateCommand();
		string sqlQuery = "SELECT *" + " FROM Users";
		dbcmd.CommandText = sqlQuery;
		IDataReader reader = dbcmd.ExecuteReader();
		while(reader.Read())
		{
			var Id = reader.GetString(0);
			var Nick = reader.GetString(1);
			var Password = reader.GetString(2);
			var Email = reader.GetString(3);
			Debug.Log("Id = "+Id+" Nick = "+ Nick+ " Password = " + Password + " Email = " + Email);
		}
		reader.Close();
     	reader = null;
     	dbcmd.Dispose();
     	dbcmd = null;
     	dbConn.Close();
     	dbConn = null;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
