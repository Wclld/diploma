﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddTrackerButton : MonoBehaviour {

    public GameObject SampleTracker;
    public float LastSampleTrackerYPosition = 110;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void OnClick()
    {
        var trackers = GameObject.Find("Trackers").transform;
        LastSampleTrackerYPosition -= 30;
        SampleTracker.transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, LastSampleTrackerYPosition);
        gameObject.transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(-145, LastSampleTrackerYPosition-35);
        Instantiate(SampleTracker,trackers);
    }
}
