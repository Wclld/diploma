﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonHelper : MonoBehaviour {
    // Use this for initialization

    private string ButtonChildComponentName = "Image";

    void Start () {
        
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void PointerDown()
    {
        var backgroundImage = gameObject.transform.Find(ButtonChildComponentName).gameObject;
        gameObject.GetComponent<AudioSource>().Stop();
        gameObject.GetComponent<AudioSource>().Play();
        backgroundImage.GetComponent<Image>().color = Color.grey;

    }
    public void PointerUp()
    {

        var backgroundImage = gameObject.transform.Find(ButtonChildComponentName).gameObject;
        backgroundImage.GetComponent<Image>().color = Color.black;

    }
}
