﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveSequenceButtonHelper : MonoBehaviour {

    public void Oclick()
    {
        var parent = gameObject.transform.parent.gameObject;
        GetAllUp(parent);
        Destroy(parent);
    }

    void GetAllUp(GameObject parent)
    {
        var parentsPrent = parent.transform.parent;
        bool elementFound = false;
        

        for (int i = 0; i < parent.transform.parent.childCount; i++)
        {
            var prevPosition = parentsPrent.GetChild(i).GetComponent<RectTransform>().anchoredPosition;

            if (elementFound)
            {
                parentsPrent.GetChild(i).GetComponent<RectTransform>().anchoredPosition = new Vector2(0, prevPosition.y - 30);
            }
            else 
            if (parentsPrent.GetChild(i) == parent)
                    elementFound = true;
        }
    }
}
