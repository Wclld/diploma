﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.UI;

public class AuthorizationHelper : MonoBehaviour {

	string conn;
	IDbConnection dbConn;
	IDbCommand dbcmd;
	public Text ErrrorMessage;

	GameObject Tabs;
	GameObject TabSelector;
	ErrorChecker checker;
	
	// Use this for initialization
	void Start () {
		Tabs = GameObject.Find("Tabs");
		Tabs.transform.localPosition = new Vector3(350,0,0);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Authorize()
	{
		ErrrorMessage.text = null;
		var autho = GameObject.Find("Authorization");
		User user = new User();
		user.Name = autho.transform.Find("NameInput").GetComponent<InputField>().text;
		user.Password = autho.transform.Find("PassInput").GetComponent<InputField>().text;

		if(String.IsNullOrEmpty(conn) )
		{
			try{
				ConnectToDatabase();
			}
			catch(Exception ex)
			{
				ErrrorMessage.text = ex.Message;	
			}

		}
		if(String.IsNullOrEmpty(ErrrorMessage.text))
		{
			try
			{
				checker = AuthorizationCheck(user);
			
			

				if(checker.Passed)
				{
					Tabs.transform.localPosition = new Vector3(0,0,0);
					TabSelector = GameObject.Find("TabSelector");
					
					if (!TabSelector)
					{
						var Canvas = GameObject.Find("Canvas");
						TabSelector = gameObject.GetComponent<TabCreator>().Tabs;
						Instantiate(TabSelector,Canvas.transform);
						GameObject.Find("Main Camera").GetComponent<AudioSource>().Pause();
					}
					ErrrorMessage.text = "";
					autho.transform.Find("PassInput").GetComponent<InputField>().text = "";
					
				}
				else
				{
						ErrrorMessage.text = checker.ErrorMessage;
				}
			}
			catch(Exception ex)
			{
				ErrrorMessage.text = conn;
			}
		}
		
		
	}

	public void Register()
	{
		var registration = GameObject.Find("Registration");

		var name = registration.transform.Find("NameInput").GetComponent<InputField>().text;
		var email = registration.transform.Find("EmailInput").GetComponent<InputField>().text;
		var password = registration.transform.Find("PassInput").GetComponent<InputField>().text;
		User newUser = new User(name,email,password);

		if(String.IsNullOrEmpty(conn) )
			ConnectToDatabase();
		ErrorChecker checker = RegistrationCheck(newUser);
		if(checker.Passed)
		{
			RegisterNewUser(newUser);
			Tabs.transform.localPosition = new Vector3(350,0,0);
		}
		else
			ErrrorMessage.text = checker.ErrorMessage;

		
		
	}


	ErrorChecker RegistrationCheck(User user)
	{
		string errorMsg;
		if(String.IsNullOrEmpty(user.Name) || String.IsNullOrEmpty(user.Email) || String.IsNullOrEmpty(user.Password))
		{
			errorMsg = "You got unfilled fields.\nFill them!";
			return new ErrorChecker(false,errorMsg);
		}
		dbcmd = dbConn.CreateCommand();
		string sqlQuery = string.Format(@"SELECT count(*)
										  FROM Users
										  where NickName = '{0}'", 
										  user.Name);
		dbcmd.CommandText = sqlQuery;
		if (ExistanceCheck())
		{
			errorMsg = "This UserName is already registered";
			return new ErrorChecker(false,errorMsg);
		}
		sqlQuery = string.Format(@"SELECT count(*)
							       FROM Users
							       where Email = '{0}'", 
							       user.Email);
		if (ExistanceCheck())
		{
			errorMsg = "This Email is already registered.";
			return new ErrorChecker(false,errorMsg);
		}

		return new ErrorChecker(true,"");
	}
	ErrorChecker AuthorizationCheck(User user)
	{
		string errorMsg = "";
		if(String.IsNullOrEmpty(user.Name) || String.IsNullOrEmpty(user.Password))
		{
			errorMsg = "No UserName or password were inputed";
			return new ErrorChecker(false,errorMsg);
		}
		dbcmd = dbConn.CreateCommand();
		string sqlQuery = string.Format(@"SELECT count(*)
										  FROM Users
										  where NickName = '{0}'
										  and Password = '{1}'",user.Name,user.Password);
		dbcmd.CommandText = sqlQuery;
		if (!ExistanceCheck())
		{
			errorMsg = "UserName or password is incorrect";
			return new ErrorChecker(false,errorMsg);
		}
		CloseConnection();
		return new ErrorChecker(true,errorMsg);
	}

	void RegisterNewUser(User user)
	{
		//INSERT INTO `Users`(`Id`,`NickName`,`Password`,`Email`) VALUES (125,'','','');
		dbcmd = dbConn.CreateCommand();
		string sqlQuery = string.Format(@"INSERT INTO `Users`
										  (`Id`,`NickName`,`Password`,`Email`)
										  VALUES ('{0}','{1}','{2}','{3}')",
										  user.Id,user.Name,user.Password,user.Email);
		Debug.Log(sqlQuery);
		dbcmd.CommandText = sqlQuery;
		dbcmd.ExecuteScalar();
	}

	public void ConnectToDatabase()
	{
		string filePath =  Application.persistentDataPath + "/database.db";
		conn= "URI=file:" + filePath;
		if(!File.Exists(filePath))
		{
			dbConn = (IDbConnection) new SqliteConnection(conn);
			dbConn.Open();
			dbcmd = dbConn.CreateCommand();
			string sqlQuery = @"CREATE TABLE 'Users'
			 					( `Id` TEXT NOT NULL UNIQUE,
								  `NickName` TEXT NOT NULL UNIQUE,
								  `Password` TEXT NOT NULL,
								  `Email` TEXT NOT NULL UNIQUE, PRIMARY KEY(`Id`) )";
			dbcmd.CommandText = sqlQuery;
			dbcmd.ExecuteScalar();
		}
		else
		{
			dbConn = (IDbConnection) new SqliteConnection(conn);
			dbConn.Open();
		}
	}
	public void CloseConnection()
	{
     	dbcmd.Dispose();
     	dbcmd = null;
     	dbConn.Close();
     	dbConn = null;
	}
	bool ExistanceCheck()
	{
		var a = Int32.Parse(dbcmd.ExecuteScalar().ToString());
		if(a == 0)
			return false;
		return true;
	}
}

public class ErrorChecker
{
		public bool Passed { get; set; }
		public string ErrorMessage { get; set; }
		public ErrorChecker(bool passed, string errorMessage)
		{
			Passed = passed;
			ErrorMessage = errorMessage;
		}

		public ErrorChecker(ErrorChecker errorChecker)
		{
			Passed = errorChecker.Passed;
			ErrorMessage = errorChecker.ErrorMessage;
		}
}

class User
{
	public string Id { get; set; }
	public string Name { get; set; }
	public string Email { get; set; }
	public string Password { get; set; }
	
	public User(string _name,string _email,string _password)
	{
		Id = Guid.NewGuid().ToString();
		Name = _name;
		Email = _email;
		Password = _password;
		
	}
	public User()
	{
	}
}