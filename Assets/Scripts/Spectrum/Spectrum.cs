﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spectrum : MonoBehaviour {

    public GameObject Prefab;
    public GameObject Parent;
    public int NumberOfObjects = 20;
    public GameObject[] Cubes;
	// Use this for initialization
	void Start () {
        for(int i =0; i < NumberOfObjects - 1; i++)
        {
            Vector3 pos = new Vector3((float)(i * 0.15 - 2.7), -4.92f, 0);
            Instantiate(Prefab,pos,Quaternion.identity,Parent.transform);
        }
        Cubes = GameObject.FindGameObjectsWithTag("Cubes");
		
	}
	
	// Update is called once per frame
	void Update () {
        float[] spectrum = AudioListener.GetSpectrumData(1024,0, FFTWindow.Hamming);
        for (int i =0; i < NumberOfObjects; i++)
        {
            Vector3 prevScale = Cubes[i].transform.localScale;
            prevScale.y = Mathf.Lerp(prevScale.y, spectrum[i] * 30, Time.deltaTime * 30);
            Cubes[i].transform.localScale = prevScale;
        }
	}
}
