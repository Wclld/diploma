﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StepToogleChecker : MonoBehaviour {

	public static void  CheckTrackerToogle(int id)
	{
		var tracker = GameObject.Find("Trackers");

		for(int i = 0; i < tracker.transform.childCount; i++)
		{
            Debug.Log(tracker.transform.GetChild(i).GetChild(id));
			var toogleState = tracker.transform.GetChild(i).GetChild(id).GetComponent<Toggle>().isOn;
			if(toogleState)
			{
                var a = tracker.transform.GetChild(i).Find("ChangeSequence").GetComponent<AudioSource>();
                a.Play();
			}
		}
	}
}
