﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;



public class EffectSelectorHelper : MonoBehaviour {
    // Use this for initialization
    public AudioMixer AuMix;

    string ListValue;
    private bool SenderState;
    private GameObject EffectSetings;
	void Start () {
        EffectSetings = GameObject.Find("EffectSetings");
        ValueChange();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ValueChange()
    {

        ListValue = gameObject.transform.Find("Label").GetComponent<Text>().text;
        float mixerFloat;
        AuMix.GetFloat(ListValue + "Level", out mixerFloat);
        if (mixerFloat > -80.0)
            SenderState = true;
        else
            SenderState = false;
        GameObject.Find("EffectToggle").GetComponent<Toggle>().isOn = SenderState;

        if (EffectSetings.transform.childCount > 0)
            Destroy(EffectSetings.transform.GetChild(0).gameObject);
        var dropdownValue = gameObject.GetComponent<Dropdown>().value;
        Instantiate(gameObject.GetComponent<EffectSetingsHelper>().EffectArray[dropdownValue],EffectSetings.transform);
    }


}




