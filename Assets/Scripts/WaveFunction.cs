﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class WaveFunction
    {
        private static WaveFunction instance;
        private WaveFunction()
        {
        }

        public static WaveFunction getInstance()
        {
            if (instance == null)
                instance = new WaveFunction();
            return instance;
        }

        public delegate float Function(float gain, double phase);
        private Function function;
        public void SetFunction(Function _function)
        {
            function = _function;
        }
        public Function GetFunction()
        {
            return function;
        }
    }

}
