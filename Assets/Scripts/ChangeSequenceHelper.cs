﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeSequenceHelper : MonoBehaviour {

	public uint SequenceNumber = 0;
	AudioSource ASource;
	void Start () {
		ASource = gameObject.GetComponent<AudioSource>();	
		OnChange();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnChange()
	{
		var a = gameObject.GetComponent<Dropdown>().value;
		AudioClip clip = GameObject.Find("AudioClips").GetComponent<AudioClipHelper>().AClips[a];
		SetAudio(clip);
		ASource.Play();
	}

	void SetAudio(AudioClip _aSource)
	{
		ASource.clip = _aSource;
	}
}
