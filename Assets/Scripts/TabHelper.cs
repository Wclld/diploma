﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabHelper : MonoBehaviour {

	GameObject Tabs;
	// Use this for initialization
	public void Start () {
		Tabs = GameObject.Find("Tabs");

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PointerDown()
	{
		if(gameObject.name == "DrumTab")
		{
			Tabs.transform.localPosition = new Vector3(0,0,0);
		}
		else if(gameObject.name == "SynthTab")
		{
			Tabs.transform.localPosition = new Vector3(-350,0,0);
		}
		else if(gameObject.name == "SequenceTab")
		{
			Tabs.transform.localPosition = new Vector3(-700,0,0);
		}
		else if(gameObject.name == "LogOut")
		{
			Tabs.transform.localPosition = new Vector3(350,0,0);
			Destroy(transform.parent.gameObject);
		}
	}
}
