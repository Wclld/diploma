﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscelator : MonoBehaviour {
	public double freequency = 440.0;
	private double increment;
	private double phase;
	private double samplingFreequency = 48000.0;

	public float gain;

    WaveFunction waveFunction;
    void Start()
    {
        waveFunction = WaveFunction.getInstance();
    }

    public void PointerDown()
	{
		gain = 0.1f;
	}
	public void PointerUp()
	{
		gain = 0;
	}
	void OnAudioFilterRead(float[] data, int channels)
	{
		increment = freequency * 2.0 * Mathf.PI /samplingFreequency;
		for(int i = 0; i < data.Length; i+= channels)
		{
			phase += increment;
            data[i] = waveFunction.GetFunction().Invoke(gain,phase);

			if(channels == 2)
			{
				data[i +1] = data[i];
			}
			if(phase > (Mathf.PI *2))
				phase = 0.0;
		}
	}

    

}

    