﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class MetronomeHelper : MonoBehaviour {
	private bool? toogleState;
    public int Base;
    public int Step;
    public float BPM;
    public int CurrentStep = 1;
    public int SubStep = 1;
    int SubStepMax ;
    
    public int CurrentMeasure;

    private float interval;
    private float nextTime;
    float nextSoundTime;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void ToogleChange()
    {
        SubStepMax = Int32.Parse(GameObject.Find("StepCountInput").GetComponent<InputField>().text);
        toogleState = gameObject.GetComponent<Toggle>().isOn; // Как-то странно оно меняет тугл(м.б. после вызова метода), по-этому значения туглов - реверснутые
        if (toogleState == false)
        {
			BPM = (float)(gameObject.transform.parent.Find("Scrollbar").GetComponent<ScrollbarHelper>().CurValue);
            StartMetronome();
        }
        else
        if(toogleState == true)
        {
			StopMetronome();
        }
        else
        {
			StopMetronome();
            Debug.Log("ToogleState - lost!");

        }

    }

    private void StopMetronome()
    {
    	StopCoroutine("DoTick");
    }

    public void StartMetronome()
    {
        StopCoroutine("DoTick");
        CurrentStep = 1;
        SubStep = 1;
        var multiplier = Base / 4f;
        var tmpInterval = 60f / BPM;
        interval = tmpInterval / multiplier;
        nextTime = Time.time;
        StartCoroutine("DoTick");
    }

    IEnumerator DoTick()
    {
        while(true)
        {
            nextTime += interval / SubStepMax;
            
			StepToogleChecker.CheckTrackerToogle(SubStep);
            
            yield return new WaitForSeconds(nextTime - Time.time);
            
            SubStep++;
            if(SubStep > SubStepMax)
            {
                CurrentStep++;
	    		gameObject.GetComponent<AudioSource>().Play();
                SubStep = 1;
            
            }
            if (CurrentStep > Step)
            {
                CurrentStep = 1;
                CurrentMeasure++;
            }
        }
    }
}