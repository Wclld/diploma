﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class EffectToggleHelper : MonoBehaviour {
    public AudioMixer AuMix;

    private string SelectedEffect;
    void Start()
    {
        SelectedEffect = GameObject.Find("EffectDropdown").transform.Find("Label").GetComponent<Text>().text;
    }
    public void ToggleChange()
    {
        SelectedEffect = GameObject.Find("EffectDropdown").transform.Find("Label").GetComponent<Text>().text;

        if (gameObject.GetComponent<Toggle>().isOn)
            AuMix.SetFloat(SelectedEffect + "Level", 1.0f);
        else
            AuMix.SetFloat(SelectedEffect + "Level", -80.0f);

    }
}
