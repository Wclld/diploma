﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveTypeHelper : MonoBehaviour {

    WaveFunction waveFunction;
    Oscelator oscelator;
	// Use this for initialization
	void Start () {
        waveFunction = WaveFunction.getInstance();
        waveFunction.SetFunction(SinWave);
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void WaveStateChange()
	{
        if(gameObject.name == "SinWave")
        {
            waveFunction.SetFunction(SinWave);
        }
        else if (gameObject.name == "SqrWave")
        {
            waveFunction.SetFunction(SqrWave);
        }
        else if (gameObject.name == "PingPongWave")
        {
            waveFunction.SetFunction(PingPongWave);
        }

    }
    public float SinWave(float gain, double phase)
    {
        return (float)(gain * Mathf.Sin((float)phase));
    }
    public float SqrWave(float gain, double phase)
    {
        if (gain * Mathf.Sin((float)phase) >= 0)
        {
            return (float)gain * 0.6f;
        }
        else
        {
            return (-(float)gain) * 0.6f;
        }
    }

    public float PingPongWave(float gain, double phase)
    {
        return (float)(gain * (double)Mathf.PingPong((float)phase, 1.0f));
    }
}

//public class WaveFunction
//{
//    private static WaveFunction instance;
//    private WaveFunction()
//    {
//    }

//    public static WaveFunction getInstance()
//    {
//        if (instance == null)
//            instance = new WaveFunction();
//        return instance;
//    }

//    public delegate float Function();
//    private Function function;
//    public void SetFunction(Function _function)
//    {
//        function = _function;
//    }
//    public Function GetFunction()
//    {
//        return function;
//    }
//}

