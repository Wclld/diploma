﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class ScrollbarHelper : MonoBehaviour {
    public bool IsMetronome;
	public float CurValue;
    public AudioMixer AuMix;

    float MaxValue;
    float MinValue;
    float ValueRange;
    StringBuilder ExposedParametr;
    float exposedParametrValue;


    // Use this for initialization
    void Start () {
        if (!IsMetronome)
        {
            ExposedParametr = new StringBuilder();
            ExposedParametr.Append(GameObject.Find("EffectDropdown").transform.Find("Label").GetComponent<Text>().text);
            ExposedParametr.Append(gameObject.transform.parent.Find("ValueName").GetComponent<Text>().text);
            Debug.Log(ExposedParametr);
            if (ExposedParametr.ToString() == "DistortionLeveli")
            {
                AuMix.GetFloat("DistortionLeveli", out exposedParametrValue);
            }
            else
            AuMix.GetFloat(ExposedParametr.ToString(),out exposedParametrValue);

        }
        try
        {
            string MinText = gameObject.transform.parent.Find("MinValue").GetComponent<Text>().text;
            MinValue = float.Parse(MinText);
            string MaxText = gameObject.transform.parent.Find("MaxValue").GetComponent<Text>().text;
            MaxValue = float.Parse(MaxText);

        }
        catch
        {
            MinValue = 0;
            MaxValue = 200;
        }
        ValueRange = MaxValue - MinValue;
        gameObject.GetComponent<Scrollbar>().value = (exposedParametrValue - MinValue) / ValueRange;

    }

    // Update is called once per frame
    void Update () {
		
	}
	public void ValueChange()
    {
        var scrollbar = gameObject;
        var ScrollbarValue = scrollbar.GetComponent<Scrollbar>().value;
        
        CurValue = MinValue + ScrollbarValue * ValueRange;
        var outputValue = gameObject.transform.transform.GetComponentInChildren<Text>();
        outputValue.text = CurValue.ToString("F2"); 
        if(!IsMetronome)
        {
            if (ExposedParametr.ToString() == "EchoDecay"|| ExposedParametr.ToString() == "EchoWetmix" || ExposedParametr.ToString() == "EchoDrymix" || ExposedParametr.ToString() == "FlangeDrymix"|| ExposedParametr.ToString() == "FlangeWetmix")
                AuMix.SetFloat(ExposedParametr.ToString(), CurValue/100); //очень странный костыль, передает какую-то дичь в 100 раз больше, именно на эти эффекты, со стороны функции
            else
                AuMix.SetFloat(ExposedParametr.ToString(), CurValue);
            Debug.Log(CurValue);
        }
    }


}
